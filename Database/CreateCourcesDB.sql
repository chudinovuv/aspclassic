/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     6/3/2014 6:12:18 PM                          */
/*==============================================================*/


if exists (select 1
          from sysobjects
          where  id = object_id('USERLOGON')
          and type in ('P','PC'))
   drop procedure USERLOGON
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('STUDENTS') and o.name = 'FK_STUDENTS_RELATIONS_GROUPS')
alter table STUDENTS
   drop constraint FK_STUDENTS_RELATIONS_GROUPS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('TRACKS') and o.name = 'FK_TRACKS_RELATIONS_STUDENTS')
alter table TRACKS
   drop constraint FK_TRACKS_RELATIONS_STUDENTS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('GROUPS')
            and   type = 'U')
   drop table GROUPS
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('STUDENTS')
            and   name  = 'RELATIONSHIP_2_FK'
            and   indid > 0
            and   indid < 255)
   drop index STUDENTS.RELATIONSHIP_2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('STUDENTS')
            and   type = 'U')
   drop table STUDENTS
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('TRACKS')
            and   name  = 'RELATIONSHIP_1_FK'
            and   indid > 0
            and   indid < 255)
   drop index TRACKS.RELATIONSHIP_1_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TRACKS')
            and   type = 'U')
   drop table TRACKS
go

/*==============================================================*/
/* Table: GROUPS                                                */
/*==============================================================*/
CREATE TABLE GROUPS (
   GROUP_ID             int                  not null,
   MO_TIME              datetime             null,
   TU_TIME              datetime             null,
   WE_TIME              datetime             null,
   TH_TIME              datetime             null,
   FR_TIME              datetime             null,
   SA_TIME              datetime             null,
   SU_TIME              datetime             null,
   constraint PK_GROUPS PRIMARY KEY nonclustered (GROUP_ID)
)
go

/*==============================================================*/
/* Table: STUDENTS                                              */
/*==============================================================*/
CREATE TABLE STUDENTS (
   STUDENT_ID           int                  identity(1, 1),
   GROUP_ID             int                  null,
   USER_LOGIN           nvarchar(50)         null,
   constraint PK_STUDENTS PRIMARY KEY nonclustered (STUDENT_ID)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_2_FK on STUDENTS (
GROUP_ID ASC
)
go

/*==============================================================*/
/* Table: TRACKS                                                */
/*==============================================================*/
CREATE TABLE TRACKS (
   STUDENT_ID           int                  not null,
   TO_LOG_IN            datetime             not null default getdate()
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_1_FK on TRACKS (
STUDENT_ID ASC
)
go

ALTER TABLE STUDENTS
   ADD CONSTRAINT FK_STUDENTS_RELATIONS_GROUPS FOREIGN KEY (GROUP_ID)
      REFERENCES GROUPS (GROUP_ID)
go

ALTER TABLE TRACKS
   ADD CONSTRAINT FK_TRACKS_RELATIONS_STUDENTS FOREIGN KEY (STUDENT_ID)
      REFERENCES STUDENTS (STUDENT_ID)
go


create procedure USERLOGON 
    @Login nvarchar(50),
    @Pwd nvarchar(50),
    @IsOk bit output,
    @IsAdmin bit output
as
    declare @StudentId int;
begin
    set @IsOk = 1;
    set @IsAdmin = 1;

    select @StudentId=STUDENT_ID
    from STUDENTS
    where USER_LOGIN=@Login;

    if(@StudentId is null)
    begin
        insert into STUDENTS(USER_LOGIN)
        values(@Login);
        
        SET @StudentId = SCOPE_IDENTITY();
    end;
  
    insert into Tracks(Student_Id)
    values(@StudentId);
end
go

